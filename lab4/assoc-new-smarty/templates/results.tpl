<!DOCTYPE HTML>
<!-- Results page of associative array search example. -->
<html>
<head>
    <title>Associative array search results page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="styles/wp.css">
</head>

<body>
<h2>Library Users</h2>
  <h3>Results for '{$query}'</h3>

{if (count($users) == 0)} 
  <p>No results found.</p>
{else}
  <table class="bordered">
  <thead>
  <tr><th>Name</th><th>Address</th><th>Phone</th><th>Email</th></tr>
  </thead>
  <tbody>
  {foreach $users as $user}
    <tr><td>{$user.name}</td><td>{$user.address}</td><td>{$user.phone}</td><td>{$user.email}</td></tr>
  {/foreach}
  </tbody>
  </table>
{/if}

<p><a href="index.php">New search</a></p>

<hr>
<p>
  Sources:
  <a href="show.php?file=results.php">results.php</a>
  <a href="show.php?file=templates/results.tpl">templates/results.tpl</a>
  <a href="show.php?file=includes/defs.php">includes/defs.php</a>
  <a href="show.php?file=includes/pms.php">includes/users.php</a>
</p>

</body>
</html>
