<?php /* Smarty version Smarty-3.1.16, created on 2014-03-26 00:08:36
         compiled from "./templates/results.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1748226439532c2efb8a2f73-73979341%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb8a6209a0e8563c0a055e398e0bc7b378fc0d87' => 
    array (
      0 => './templates/results.tpl',
      1 => 1395792508,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1748226439532c2efb8a2f73-73979341',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_532c2efb913aa8_46179943',
  'variables' => 
  array (
    'query' => 0,
    'users' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_532c2efb913aa8_46179943')) {function content_532c2efb913aa8_46179943($_smarty_tpl) {?><!DOCTYPE HTML>
<!-- Results page of associative array search example. -->
<html>
<head>
    <title>Associative array search results page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="styles/wp.css">
</head>

<body>
<h2>Library Users</h2>
  <h3>Results for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h3>

<?php if ((count($_smarty_tpl->tpl_vars['users']->value)==0)) {?> 
  <p>No results found.</p>
<?php } else { ?>
  <table class="bordered">
  <thead>
  <tr><th>Name</th><th>Address</th><th>Phone</th><th>Email</th></tr>
  </thead>
  <tbody>
  <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
    <tr><td><?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['user']->value['address'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['user']->value['phone'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
</td></tr>
  <?php } ?>
  </tbody>
  </table>
<?php }?>

<p><a href="index.php">New search</a></p>

<hr>
<p>
  Sources:
  <a href="show.php?file=results.php">results.php</a>
  <a href="show.php?file=templates/results.tpl">templates/results.tpl</a>
  <a href="show.php?file=includes/defs.php">includes/defs.php</a>
  <a href="show.php?file=includes/pms.php">includes/users.php</a>
</p>

</body>
</html>
<?php }} ?>
