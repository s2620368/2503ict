<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "users.php";
date_default_timezone_set('utc');

/* Search sample data for $name or $address or $phone or $email from form. */
function search($query) {
  global $users; 
  
  // Filter $pms by $name
  if (!empty($query)) {
    $results = array();
    foreach ($users as $user) {
      if ((stripos($user['name'], $query) !== FALSE) || (stripos($user['address'], $query) !== FALSE || strpos($user['phone'], $query) !== FALSE) || (stripos($user['email'], $query) !== FALSE) ) {
        $results[] = $user;
      }
    }
    $users = $results;
  }
  return $users;
          }
?>
