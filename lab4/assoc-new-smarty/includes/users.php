<?php
/* Library Users.  */


$users = array(
array('name' => 'Julia Gillard', 'address' => 'Loganlea Rd, Logan', 'phone' => '1234 4321', 'email' => 'julia@the.lodge'),
array('name' => 'Tony Abbott', 'address' => 'Logan Rd, Mt Gravatt', 'phone' => '9876 5432', 'email' => 'tony@the.hermitage'),
array('name' => 'Christine Milne', 'address' => 'Kessels Rd, Nathan','phone' => '91234 5678', 'email' => 'chris@the.treetop'),
array('name' => 'Burt Stevens', 'address' => 'Mountain Rd, Glenelg','phone' => '0405 9657', 'email' => 'burt@the.mountain'),
array('name' => 'Angus Peters', 'address' => 'Garden Rd, St Andrews','phone' => '8765 9631', 'email' => 'angus@the.garden'),
); 

?>
