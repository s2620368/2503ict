<?php /* Smarty version Smarty-3.1.16, created on 2014-03-28 06:23:43
         compiled from "./templates/form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9440652665335156fd6f611-10167270%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '57ed4ca47fb865220b8ffe94f63dd30b17e3fc15' => 
    array (
      0 => './templates/form.tpl',
      1 => 1395985791,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1395987808,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9440652665335156fd6f611-10167270',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5335156fde41d6_95221067',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5335156fde41d6_95221067')) {function content_5335156fde41d6_95221067($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <title>Job Search – Add Job</title>
  
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Search</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  
  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Job Search</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Sign In</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="">List all Jobs</a>
            <a class="list-group-item" href="">Search for a Job</a>
            <a class="list-group-item" href="">List all Employees</a>
            <a class="list-group-item" href="form.php">Advertise a Job</a>
          </div>
        </div>
        
<div class="col-sm-9">
          <h2 id="heading">Search a Job!</h2><br>
          <form class="form-horizontal" role="form">
            <div class="form-group">
              <label class="col-sm-2"> Title </label>
              <div class="col-sm-10"> <input type="text" id="title"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Company </label>
              <div class="col-sm-10"> <input type="text" id="company"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Salary </label>
              <div class="col-sm-10"> <input type="text" id="salary"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Closing Date </label>
              <div class="col-sm-10"> <input type="text" id="closes"> </div>
            </div>
            <button type="submit" class="btn btn-success"> Submit </button>
          </form>
        </div>

      </div>
    </div>
  
</body>
</html><?php }} ?>
