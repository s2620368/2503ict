{extends file='layout.tpl'}
{block name=title}Job Search – Add Job{/block}

{block name=body1}
<div class="col-sm-9">
          <h2 id="heading">Search a Job!</h2><br>
          <form class="form-horizontal" role="form">
            <div class="form-group">
              <label class="col-sm-2"> Title </label>
              <div class="col-sm-10"> <input type="text" id="title"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Company </label>
              <div class="col-sm-10"> <input type="text" id="company"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Salary </label>
              <div class="col-sm-10"> <input type="text" id="salary"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Closing Date </label>
              <div class="col-sm-10"> <input type="text" id="closes"> </div>
            </div>
            <button type="submit" class="btn btn-success"> Submit </button>
          </form>
        </div>
{/block}