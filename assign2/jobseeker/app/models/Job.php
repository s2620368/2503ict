<?php
use Carbon\Carbon;
class Job extends Eloquent{

  //rules for job table validation
  public static $rules = array(
    'job_title' => 'required',
    'job_desc' => 'required',
    //'user_id'  => 'required',
    'location' => 'required',
    'salary' => 'required|integer',
    'start_date' => 'required',
    'final_date' => 'required'
  );

  /*
   * one to many relationship between users and jobs
   * jobs only have one user(creator)
   * but they have many users applying ??????
   */
  public function user(){
    return $this->belongsTo('User');
  }
  public function application(){
    return $this->hasMany('Application');
  }



  public function getDates(){
    return array('created_at','updated_at', 'start_date', 'final_date');
  }

}