<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
  /*
   * one to many relationship between users and jobs
   * users have many jobs
   */
  public function jobs(){
    return $this->hasMany('Job');
  }
  public function applications(){
    return $this->hasMany('Application');
  }
  public function getDates(){
    return array('created_at','updated_at', 'start_date', 'final_date');
  }
  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
  protected $table = 'users';

  /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
  protected $hidden = array('password');

  /**
	 * Rules for validation
	 * Create and login
	 */
  public static $create_rules = array(
    'username' => 'required|unique:users',
    'password' => 'required',
    'name' => 'required'
  );
  public static $login_rules = array(
    'username' => 'required',
    'password' => 'required|confirmed'
  );

  /*
  * for images
  *
  */
  use Codesleeve\Stapler\Stapler;

  public function __construct(array $attributes = array()) {
    $this->hasAttachedFile('profilePic', [
      'styles' => [
        'medium' => '300x300',
        'thumb' => '100x100'
      ]
    ]);

    parent::__construct($attributes);
  }

  /**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
  public function getAuthIdentifier()
  {
    return $this->getKey();
  }

  /**
	 * Get the password for the user.
	 *
	 * @return string
	 */
  public function getAuthPassword()
  {
    return $this->password;
  }

  /**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
  public function getRememberToken()
  {
    return $this->remember_token;
  }

  /**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
  public function setRememberToken($value)
  {
    $this->remember_token = $value;
  }

  /**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
  public function getRememberTokenName()
  {
    return 'remember_token';
  }

  /**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
  public function getReminderEmail()
  {
    return $this->email;
  }

}
