<?php

class Application extends Eloquent{

  public function user(){
    return $this->belongsTo('User');
  }
  public function job(){
    return $this->belongsTo('Job');
  }
  public function getDates(){
    return array('created_at','updated_at', 'start_date', 'final_date');
  }

}

