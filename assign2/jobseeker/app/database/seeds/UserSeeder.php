<?php

class UserSeeder extends Seeder {
  public function run()
  {
    $user = new User;
    $user->username = 'meat@gmail.com';
    $user->password = Hash::make('butcher27');
    $user->category = 2;
    $user->name = 'The Bacon Brothers';
    $user->email = 'meat@gmail.com';
    $user->phone = '07 5563 896';
    $user->industry = 'Food Production';
    $user->description = 'For all your meat needs';
    $user->remember_token = "default";
    $user->save();

    $user = new User;
    $user->username = 'hire@gmail.com';
    $user->password = Hash::make('truck27');
    $user->category = 2;
    $user->name = 'Truck Hire Ltd';
    $user->email = 'hire@gmail.com';
    $user->phone = '07 889 563';
    $user->industry = 'roads and construction';
    $user->description = 'Truck and Labour Hire';
    $user->remember_token = "default";
    $user->save();

    $user = new User;
    $user->username = 'angela@gmail.com';
    $user->password = Hash::make('ange27');
    $user->category = 1;
    $user->name = 'Angela King';
    $user->email = 'angela@gmail.com';
    $user->phone = '0403 544 342';
    $user->description = 'Looking for a job to get money, I dont have any skills';
    $user->remember_token = "default";
    $user->save();

    $user = new User;
    $user->username = 'bob@gmail.com';
    $user->password = Hash::make('bob27');
    $user->category = 1;
    $user->name = 'Bob Stevens';
    $user->email = 'bob@gmail.com';
    $user->phone = '0455 89 789';
    $user->description = 'I am unmotivated, I am not good at working in a team';
    $user->remember_token = "default";
    $user->save();

  }
}