<?php

class JobSeeder extends Seeder {
  public function run()
  {
    $job = new Job;
    $job->job_title = 'Butcher';
    $job->user_id = 1;
    $job->job_desc = 'Postion Available for full-time Butcher';
    $job->location = 'Nerang';
    $job->salary = 52000;
    $job->start_date = '2014-05-05';
    $job->final_date = '2014-06-02';
    $job->save();

    $job = new Job;
    $job->job_title = 'Truck Driver';
    $job->user_id = 2;
    $job->job_desc = 'Do you love cats? This job is for you!';
    $job->location = 'Tweed Heads';
    $job->salary = 22000;
    $job->start_date = new DateTime('6 May 2014');
    $job->final_date = '2014-06-09';
    $job->save();

    $job = new Job;
    $job->job_title = 'Labourer';
    $job->user_id = 2;
    $job->job_desc = 'Required for casual position, must have current permits/licenses';
    $job->location = 'Gold Coast / Brisbane areas';
    $job->salary = 75000;
    $job->start_date = '2014-05-14';
    $job->final_date = '2014-06-11';
    $job->save();

    $job = new Job;
    $job->job_title = 'Sales Clerk';
    $job->user_id = 1;
    $job->job_desc = 'Busy supermarket requires a part-time cash register attendant';
    $job->location = 'Nerang';
    $job->salary = 22000;
    $job->start_date = '2014-05-22';
    $job->final_date = '2014-06-19';
    $job->save();

  }
}