<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration {

  /**
	 * Run the migrations.
	 *
	 * @return void
	 */
  public function up()
  {
    //
    Schema::create('applications', function($table){
      $table->increments('id');
      $table->integer('job_id');
      $table->integer('user_id');
      $table->string('userName');
      $table->string('employerName');
      $table->text('letter');
      $table->date('apply_date');
      $table->timestamps();
    });
  }

  /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  public function down()
  {
    //
    Schema::drop('applications');
  }

}
