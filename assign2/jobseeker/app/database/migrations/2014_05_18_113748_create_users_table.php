<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

  /**
	 * Run the migrations.
	 *
	 * @return void
	 */
  public function up()
  {
    //
    Schema::create('users', function($table) {
      $table->increments('id');
      // category: 1=jobseeker, 2=employer
      $table->integer('category');
      $table->string('username')->unique();
      $table->string('password')->index();
      $table->string('name');
      $table->string('email');
      $table->string('phone');
      $table->string('industry')->nullable = true;
      $table->string('description');
      $table->string('remember_token');
      $table->timestamps();
    });
  }

  /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  public function down()
  {
    //
    Schema::drop('users');
  }

}
