<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddProfilePicFieldsToUsersTable extends Migration {

  /**
	 * Make changes to the table.
	 *
	 * @return void
	 */
  public function up()
  {	
    Schema::table('users', function(Blueprint $table) {		

      $table->string("profilePic_file_name")->nullable();
      $table->integer("profilePic_file_size")->nullable();
      $table->string("profilePic_content_type")->nullable();
      $table->timestamp("profilePic_updated_at")->nullable();

    });

  }

  /**
	 * Revert the changes to the table.
	 *
	 * @return void
	 */
  public function down()
  {
    /*
    Schema::table('users', function(Blueprint $table) {

			$table->dropColumn("profilePic_file_name");
			$table->dropColumn("profilePic_file_size");
			$table->dropColumn("profilePic_content_type");
			$table->dropColumn("profilePic_updated_at");

		});
    */
  }

}