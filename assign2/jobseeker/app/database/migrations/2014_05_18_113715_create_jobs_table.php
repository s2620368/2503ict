<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

  /**
	 * Run the migrations.
	 *
	 * @return void
	 */
  public function up()
  {
    //
    Schema::create('jobs', function($table){
      $table->increments('id');
      $table->string('job_title');
      $table->integer('user_id');
      $table->text('job_desc');
      $table->string('location');
      $table->integer('salary');
      $table->date('start_date');
      $table->date('final_date');
      $table->timestamps();
    });
  }

  /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  public function down()
  {
    //
    Schema::drop('jobs');
  }

}
