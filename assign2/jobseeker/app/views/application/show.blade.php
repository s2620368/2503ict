@extends('job.layout')

@section('content')
  <img src="../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <div class="col-sm-9">
    @if ((Auth::user()->category) == 1)
      <h1 id="heading">You applied for "{{{$applications->job->job_title}}}"</h1>
      <br>Employer: {{{ $applications->employerName}}}
      <p>Letter: {{{$applications->letter}}}</p>
      <p>Date of Application: {{{ $applications->apply_date }}}</p>
    @else
      <h1 id="heading">Application for "{{{$applications->job->job_title}}}"</h1>
      <br>Name of Applicant: {{{ $applications->userName}}}
      <p>Letter: {{{$applications->letter}}}</p>
      <p>Date of Application: {{{ $applications->apply_date }}}</p>
      <br>
      <p>{{link_to_route('user.show', 'View Profile Applicants Profile', array($applications->user->id)) }}
    @endif   
  </div>
@stop