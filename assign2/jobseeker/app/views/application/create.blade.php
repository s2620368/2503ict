@extends('job.layout')

@section('content')
<img src="../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <div class="col-sm-9">
    <h1 id="heading">Apply for  Vacancy</h1>
    <p>Hi {{ Auth::user()->name }}, please fill in the form below to apply for the following vacancy</p>
    <p>Job Title: {{$jobs->job_title}}</p> 
    <p>Employer: {{$jobs->user->name}}<img src="{{ asset($jobs->user->profilePic->url('thumb')) }}"></p>
      {{ Form::open(array('route' => 'application.store')) }}
      {{ Form::hidden('user_id', Auth::user()->id )}}
      {{ Form::hidden('userName', Auth::user()->name) }}
      {{ Form::hidden('employerName', $jobs->user->name) }}
      {{ Form::hidden('job_id', $jobs->id) }}
    <p>
      {{ Form::label('letter', 'Application Letter:', array('class' => 'col-sm-3')) }}
      {{ Form::textarea('letter') }}
    <p>
      {{ Form::label('apply_date', 'Application Date:', array('class' => 'col-sm-3')) }}
      {{ Form::text('apply_date', null, array('placeholder' => 'YYYY-mm-dd')) }}
    <p>
      {{ Form::submit('Apply Now', array('class' => 'btn btn-success')) }}
      {{ Form::close() }}
  </div>
@stop