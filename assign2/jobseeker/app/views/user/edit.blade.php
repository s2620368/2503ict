@extends('job.layout')

@section('content')
  <img src="../../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <div class="col-sm-9">
  <h1 id="heading">Edit Profile</h1>
    {{ Form::model($user, array('method' => 'PUT',  'route' => array('user.update', $user->id),'files' => true)) }}
  <p>'Hi {{ Auth::user()->name }}, please update your details:'  </p>
  <br>
    {{ Form::label('name', 'Name:') }}
    {{ Form::text('name') }}
  <p>
    {{ Form::label('description', 'Description:') }}
    {{ Form::text('description') }}
  <p>
    {{ Form::label('phone', 'Phone:') }}
    {{ Form::text('phone') }}
    {{ $errors->first('location') }}
  <p>
    {{ Form::label('profilePic', 'Upload your Company Logo:') }}
    {{ Form::file('profilePic') }}
  <p>
    {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}
</div>
@stop