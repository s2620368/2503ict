@extends('job.layout')

@section('content')
  <img src="../images/logo.png" alt="Logo Oz Vacancies">
  @if($user->category == 2)
  <p>{{ link_to_route('job.create','Add new Job') }}
  @endif
@stop

@section('content1')
  <br>
  <h1 id="heading">Profile for "{{{$user->name}}}"</h1>
  <br> 
  <dl>
    <dt id="heading">Email:</dt>
    <dd>{{{$user->email}}}</dd>
    <dt id="heading">Phone: </dt>
    <dd>{{{$user->phone}}}</dd>
    <dt id="heading">About Us:</dt>
    <dd>{{{$user->description}}}</dd>
    <dt id="heading">Profile Picture:</dt>
    <dd><img src="{{ asset($user->profilePic->url('thumb')) }}"></dd>
  </dl> 
  <br>
  <br>
  <p>{{link_to_route('user.edit', 'Update Profile', array($user->id)) }} </p>
  <br>
    {{ Form::open(array('method' => 'DELETE', 'route' =>array('user.destroy', $user->id))) }}
    {{ Form::submit('Delete Account ', array('class' => 'btn btn-warning')) }}
    {{ Form::close() }}
    <br>
    @if ($user->category ==1)
      <p>You've applied for these jobs:</p>
        @if ($applications != null)  
          <ul>
            @foreach ($applications as $application)
              <li>{{link_to_route('application.show', $application->job->job_title, array($application->id)) }}</li>
            @endforeach
          </ul>
        @endif
    @else
      <p>Jobs you have listed:</p>
        <ul>
          @foreach ($jobs as $job)
        <li>{{link_to_route('job.show',$job->job_title, array($job->id)) }}</li>
      @endforeach
    </ul>
  @endif
@stop