@extends('job.layout')

@section('content')
  <img src="../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
<div class="col-sm-9">
    @if($category == 2)
      <h1 id="heading">New Employer page</h1>
      <br>
      <p>{{ Form::open(array('route' => 'user.store', 'files' => true)) }}
      <p>{{ Form::label('username', 'Username:', array('class' => 'col-sm-4')) }}
      <p>{{ Form::text('username', null, array('class' => 'input-small', 'placeholder' => 'Email')) }}
      <p>{{$errors->first('username')}}
      <p>{{ Form::label('password', 'Password:', array('class' => 'col-sm-4')) }}
      <p>{{ Form::password('password', array('class' => 'input-small', 'placeholder' => 'Password')) }}
      <p>{{$errors->first('password') }}
      {{ Form::hidden('category', 2) }}
      <p>{{ Form::label('name', 'Name:', array('class' => 'col-sm-4')) }}
      <p>{{ Form::text('name') }}
      <p>{{$errors->first('name') }}
      <p>{{ Form::label('description', 'Company Description:', array('class' => 'col-sm-4')) }}
      <p>{{ Form::textarea('description') }}
      <p>{{ Form::label('industry', 'Industry', array('class' => 'col-sm-4')) }}
      <p>{{ Form::text('industry') }}
      <p>{{ Form::label('phone', 'Phone Number:', array('class' => 'col-sm-4')) }}
      <p>{{ Form::text('phone') }}
      <p>{{$errors->first('phone') }}
      <br>
      <p>{{ Form::label('profilePic', 'Upload your Company Logo:') }}
      <p>{{ Form::file('profilePic') }}
      <br>
      <p>{{ Form::submit('Create Account', array('class' => 'btn btn-info')) }}
      {{ Form::close() }}
    @else
      <h1 id="heading">New Job Seeker page</h1>
      <br>
      <p>{{ Form::open(array('route' => 'user.store', 'files' => true)) }}
      <p>{{ Form::label('username', 'Username:', array('class' => 'col-sm-3')) }}
      <p>{{ Form::text('username', null, array('class' => 'input-small', 'placeholder' => 'Email')) }}
      <p>{{$errors->first('username')}}
      <p>{{ Form::label('password', 'Password:', array('class' => 'col-sm-3')) }}
      <p>{{ Form::password('password', array('class' => 'input-small', 'placeholder' => 'Password')) }}
      <p>{{$errors->first('password') }}
      <p>{{ Form::hidden('category', 1) }}
      <p>{{ Form::hidden('industry', null) }}
      <p>{{ Form::label('name', 'Name:', array('class' => 'col-sm-3')) }}
      <p>{{ Form::text('name') }}
      <p>{{$errors->first('name') }}
      <p>{{ Form::label('description', 'About You:', array('class' => 'col-sm-3')) }}
      <p>{{ Form::textarea('description') }}
      <br><p>
      <p>{{ Form::label('phone', 'Phone Number:', array('class' => 'col-sm-3')) }}
      <p>{{ Form::text('phone') }}
      <p>{{$errors->first('phone') }}
      <p>{{ Form::label('profilePic', 'Upload your Profile Picture:') }}
      {{ Form::file('profilePic') }}
      <br><p>
      <p>{{ Form::submit('Create Account', array('class' => 'btn btn-info')) }}
      {{ Form::close() }}
    @endif
  </div>
@stop