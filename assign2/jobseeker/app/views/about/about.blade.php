<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oz Vacancies</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <!-- Add custom CSS here -->
    {{ HTML::style('styles/style.css')}}
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href=""><!--Oz Vacancies<img src="images/jobwanted.jpg" alt="Job Wanted Picture">--></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li>{{link_to_route('job.index', 'Home' )}}</li>
            <li><a href=""></a></li>
            <li>{{link_to_route('about.about', 'About' )}}</li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <h3 id="title">Angela King s2620368<br>
          2503ICT Web Programming Assignment 2    <img src="../images/logo.png" alt="Logo Oz Vacancies"><br> 
        Due: Monday 2nd June 2014 11:59pm </h3>
        <br>
        <br>
    <p>    
    Oz Vacancies is a job seeker website designed for job seekers (users) to either see a full list of jobs or to enter a search<br>
    criteria to search for jobs. It also has an Employer section so employers can add new jobs, update existing jobs or delete jobs.<br>
    <br>
    The inital home screen has a list of recently added jobs on the left, with a link to see all jobs below and login forms on the right. If a user <br>
    (seeker or employer) is registered they can login or if they are not registered, there are links for Jobseekers or Employers to register. There is a search <br>
    function where users can search for industry, location, minimum salary or a keyword in the job description. A results search page is then shown.<br>
    <br>
    When a Jobseeker is logged in, they can click on 'View Profile' and see their profile details and a list of jobs they have applied for. The can also <br>
    update their details or delete their account.  When a Jobseeker clicks on a job to view it, they are able to click 'Apply' and fill in an application.<br> 
    <br>
    When an Employer is logged in the are also able to view, update and delete their profile, they can see a list of jobs they have listed. If they click<br>
    on one of thier own jobs, they are able to update or delete it, they are also able to see a list of Jobseekers who have applied for that particular job<br>
    with links to view the application and profile of person who made it.<br>
    <br>
    </p>
    The following is the database schema:<br>
        <h5>Jobs Table:</h5>
        <pre>
            Schema::create('jobs', function($table){
              $table->increments('id');
              $table->string('job_title');
              $table->integer('user_id');
              $table->text('job_desc');
              $table->string('location');
              $table->integer('salary');
              $table->date('start_date');
              $table->date('final_date');
              $table->timestamps();
            });
        </pre>
         <h5>Users Table:</h5>
        <pre>
            Schema::create('users', function($table) {
              $table->increments('id');
              // category: 1=jobseeker, 2=employer
              $table->integer('category');
              $table->string('username')->unique();
              $table->string('password')->index();
              $table->string('name');
              $table->string('email');
              $table->string('phone');
              $table->string('industry')->nullable = true;
              $table->string('description');
              $table->string('remember_token');
              $table->timestamps();
            });
        </pre>
        <h5>Applications Table:</h5>
        <pre>
            Schema::create('applications', function($table){
              $table->increments('id');
              $table->integer('job_id');
              $table->integer('user_id');
              $table->string('userName');
              $table->string('employerName');
              $table->text('letter');
              $table->date('apply_date');
              $table->timestamps();
            });
        </pre>
        <p>
        Separate home pages for Users and Employers: There is only one home page which is the same for both User and Employers.<br>
        Which is here: {{link_to_route('job.index', 'Home' )}}<br>
        <br>
        <h5>Users:</h5>

        <table style="width:300px">
          <tr>
            <th>username</th>
            <th>password</th>
            <th>Category</th>
          </tr>
          <tr>
            <td>hire@gmail.com</td>
            <td>truck27</td>
            <td>Employer</td>
          </tr>
          <tr>
            <td>meat@gmail.com</td>
            <td>butcher27</td>
            <td>Employer</td>
          </tr>
          <tr>
            <td>nurses@gmail.com</td>
            <td>nurses27</td>
            <td>Employer</td>
          </tr>
          <tr>
            <td>angela@gmail.com</td>
            <td>ange27</td>
            <td>User</td>
          </tr>
          <tr>
            <td>burt@gmail.com</td>
            <td>burt27</td>
            <td>User</td>
          </tr>
          <tr>
            <td>bob@gmail.com</td>
            <td>bob27</td>
            <td>User</td>
          </tr>
        </table> 
        <p><br>
          <h5>Requirements/Features not impletemented or working properly:</h5>
        <dl>
          <dt>Sanitisation</dt>
          <dd> - I have used "Triple Curley Braces" where I should but am still getting errors, and jobs are still saving with "Title"</dd>
          <dt>Users can only apply for job once</dt>
          <dd> - This has not been implemented, I tried a few different things but could not get it to work. I think it is do do with my model relationships, I can't seem to use the dynamic<br>
          property in some places.</dd>
          <dt>Pagination on search</dt>
          <dd> - This is working for search criteria greater than the pagination amount (set at 3), but when you click back and forward thru the pages, you lose the search data, it defaults back 
            to all jobs - and also shows the jobs that have expired. </dd>
          <dt>Number of days left to apply for job</dt>
          <dd> - This displays the days left to apply but also displays the days past final date, as a whole number, not a negative. </dd>
        </dl> 
    </div>
   </div>
  </body>
</html>