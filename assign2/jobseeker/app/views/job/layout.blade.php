<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oz Vacancies</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <!-- Add custom CSS here -->
    {{ HTML::style('styles/style.css')}}
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href=""></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li>{{link_to_route('job.index', 'Home' )}}</li>
            <li><a href=""></a></li>
            <li>{{link_to_route('about.about', 'About' )}}</li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            @if (Auth::check())
            <li>{{ link_to_route('user.logout', 'Logout (' . Auth::user()->name . ')') }}</li>
            @endif
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            @yield('content')
          </div>
        </div>
        <div class="col-sm-9">
           @if (!Auth::check())
           <div class="login-form">
              {{ Form::open(array('action' => 'UserController@login')); }}
              {{ Form::text('username', null, array('class' => 'input-small', 'placeholder' => 'Email')); }}
              {{ Form::password('password', array('class' => 'input-small', 'placeholder' => 'Password')); }}
              {{ Form::submit('Sign in', array('class' => 'btn btn-danger')); }}
              {{ Form::close(); }}
              {{ Form::open(array('action' => 'UserController@create')); }}
              <p>{{link_to_route('user.create', 'or Register here',['category' => 1])}}
              <p>{{$errors->first('username')}}
              <p>{{$errors->first('password')}}
              {{ Form::close(); }}
          </div>
          <br>
          <br>
          <br>
          <br>
          {{link_to_route('user.create', 'Employers', ['category' => 2], array('class' => 'btn btn-primary')) }}
          <p id="employer-heading">Employers, Please register here:</p>
          @else
          <p><br> {{link_to_route('user.show', 'View Profile', ['id' => Auth::user()->id], array('class' => 'btn btn-primary')) }}
          @endif
          @yield('content1') 
          <br>
          <br>
          <br>
          <br>  
        </div>
        <footer>
          Angela King s2620368<br>
          2503ICT Web Programming Assignment 2
        </footer> 
      </div>
    </div>
  </body>
</html>