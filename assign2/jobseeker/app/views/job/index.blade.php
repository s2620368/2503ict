@extends('job.layout')

@section('content')
  <img src="images/logo.png" alt="Logo Oz Vacancies">
  <h3 class="list-heading">Recently Added</h3>
    <ul>
      @foreach ($jobs as $job)
        @if ($job->final_date > $now)
          <li>{{link_to_route('job.show',$job->job_title, array($job->id)) }}</li>
        @endif
      @endforeach
    </ul>
    {{ $jobs->links()}}
    <p>{{ link_to_route('job.listAll','See all Jobs') }}
@stop

@section('content1')
  <div class="col-sm-9">
    <br><img src="images/jobwanted.jpg" alt="Job Wanted Picture">
    <h3 id="heading"> Search for a Job... </h3>
    <br>
    <div id="content">
      <p id="search">I'd like a job in:<p>
      {{ Form::open(array('method' => 'GET', 'action' => 'JobController@listAll')); }}
      {{ Form::text('keyword', null, array('placeholder' => 'search by keyword')); }}
      {{ Form::submit('Search', array('class' => 'btn btn-success')); }}
      {{ Form::close(); }}
      <p>E.g Search for position name, keyword in description, industry, location or minimum salary </p>
    </div>
 </div>
@stop