@extends('job.layout')

@section('content')
  <img src="../images/logo.png" alt="Logo Oz Vacancies">
  @if((Auth::check()) and ((Auth::user()->category) == 2))
    <p>{{ link_to_route('job.create','Add new Job') }}
  @endif
@stop

@section('content1')
  <div class="col-sm-9">
    <h1 id="heading">Vacancy for "{{{$jobs->job_title}}}"</h1>
    <img src="{{ asset($jobs->user->profilePic->url('thumb')) }}">
    <br> 
    <dl>
        <dt id="heading">Employer:</dt>
        <dd>{{{ $jobs->user->name}}}</dd>
        <dt id="heading">Job Description:</dt>
        <dd>{{{$jobs->job_desc}}}</dd>
        <dt id="heading">Location:</dt>
        <dd>{{{$jobs->location}}}</dd>
        <dt id="heading">Salary:</dt>
        <dd>{{{$jobs->salary}}}</dd>
        <dt id="heading">Start Date:</dt>
        <dd>{{{ date("d.m.Y",strtotime($jobs->start_date)) }}}</dd>
        <dt id="heading">End Date:</dt>
        <dd>{{{ date("d.m.Y",strtotime($jobs->final_date)) }}}</dd>
        <dt id="heading">Days left to Apply:</dt>
        <dd>{{{$jobs->final_date->diff($now)->days}}}</dd>
     </dl> 
      @if (Auth::check())
        @if ((Auth::user()->category) == 1)    
          @foreach ($applications as $application)
            <li>{{link_to_route('application.show', $application->userName, array($application->id)) }}</li>
            <p>Sorry you've already applied for this job</p>
          @endforeach
          @if ($now > $jobs->final_date)
            <p> Sorry, applications have now closed
          @else
            <p>{{link_to_route('application.create', 'Apply Now', ['id' => $jobs->id], array('class' => 'btn btn-primary')) }}</p>
          @endif
        @elseif ((Auth::user()->id) == ($jobs->user_id))
        <p>{{link_to_route('job.edit', 'Edit', array($jobs->id)) }} </p>
        <p>{{link_to_route('job.index', 'Home' )}}</p>
        <p>
        {{ Form::open(array('method' => 'DELETE', 'route' =>array('job.destroy', $jobs->id))) }}
        {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
        {{ Form::close() }} 
        <br>
        <p> You have received the following applications for this job:</p>
        <ul>
          @foreach ($applications as $application)
            <li>{{link_to_route('application.show', $application->userName, array($application->id)) }}</li>
          @endforeach
        <ul>
        @endif
      @else
          <p id="heading">please log in or register to apply</p>
      @endif
  </div>
@stop