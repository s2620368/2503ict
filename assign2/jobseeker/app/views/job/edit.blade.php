@extends('job.layout')

@section('content')
  <img src="../../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <div class="col-sm-9">
    <h1 id="heading">Edit Job</h1>
    <br>
      {{ Form::model($job, array('method' => 'PUT', 'route' =>array('job.update', $job->id))) }}
      {{ Form::label('job_title', 'Job Title:') }}
      {{ Form::text('job_title') }}
      {{ $errors->first('job_title') }}
    <p>
      {{ Form::label('job_desc', 'Job Description:') }}
      {{ Form::text('job_desc') }}
      {{ $errors->first('job_desc') }}
    <p>
      {{ Form::label('location', 'Location:') }}
      {{ Form::text('location') }}
      {{ $errors->first('location') }}
    <p>
      {{ Form::label('salary', 'Salary:') }}
      {{ Form::text('salary') }}
      {{ $errors->first('salary') }}
    <p>
      {{ Form::submit('Update') }}
      {{ Form::close() }}
  </div>
@stop