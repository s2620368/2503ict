@extends('job.layout')

@section('content')
  <img src="../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <h3 id="heading">All Jobs</h3>
    @if ($keyword != null)
      <p> Search results for "{{{$keyword}}}"</p>
    @endif
    @if (count($jobs) == 0)
      <p>Sorry, no jobs found matching that criteria</p>
    @else
      <ul>
        @foreach ($jobs as $job)
          @if ($job->final_date > $now)
            <li>{{link_to_route('job.show',$job->job_title, array($job->id)) }}</li>
          @endif
        @endforeach
     </ul>
    {{ $jobs->links()}}
    <br>
    <div id="content">
      <p id="search">Search Again:<p>
        {{ Form::open(array('method' => 'GET', 'action' => 'JobController@listAll')); }}
        {{ Form::text('keyword', null, array('placeholder' => 'search by keyword')); }}
        {{ Form::submit('Search', array('class' => 'btn btn-success')); }}
        {{ Form::close(); }}
        <p>E.g Search for position name, keyword in description, industry, location or minimum salary </p>
    </div>
  @endif
@stop
