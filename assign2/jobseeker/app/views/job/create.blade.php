@extends('job.layout')

@section('content')
<img src="../images/logo.png" alt="Logo Oz Vacancies">
@stop

@section('content1')
  <div class="col-sm-9">
    <h1 id="heading">Create Job</h1>
    {{ Form::open(array('action' => 'JobController@store')) }}
    <br><p>
    {{ Form::label('job_title', 'Job Title:', array('class' => 'col-sm-3')) }}
    {{ Form::text('job_title') }}
    <p>{{$errors->first('job_title')}}
    <br><p>
    {{ Form::hidden('user_id', Auth::user()->id) }}
    {{ Form::label('job_desc', 'Job Description:', array('class' => 'col-sm-3')) }}
    {{ Form::text('job_desc') }}
    <p>{{$errors->first('job_desc')}}
    <br><p>
    {{ Form::label('location', 'Location:', array('class' => 'col-sm-3')) }}
    {{ Form::text('location') }}
    <p>{{$errors->first('location')}}
    <br><p>
    {{ Form::label('salary', 'Salary:', array('class' => 'col-sm-3')) }}
    {{ Form::text('salary') }}
    <p>{{$errors->first('salary')}}
    <br><p>
    {{ Form::label('start_date', 'Start Date:', array('class' => 'col-sm-3')) }}
    {{ Form::text('start_date', null, array('placeholder' => 'YYYY-mm-dd')) }}
    <p>{{$errors->first('start_date')}}
    <br><p>
    <br><p>
    {{ Form::label('final_date', 'Finish Date:', array('class' => 'col-sm-3')) }}
    {{ Form::text('final_date', null, array('placeholder' => 'YYYY-mm-dd')) }}
    <p>{{$errors->first('final_date')}}
    <br><p>
    {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
    <br><p>
    {{ Form::close() }}
  </div>
@stop