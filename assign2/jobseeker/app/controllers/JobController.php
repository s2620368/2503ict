<?php

class JobController extends \BaseController {

  /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  public function index()
  {
    //
    $jobs = Job::whereRaw('start_date > ?',array(new DateTime('10 May 2014')))->paginate(3);
    $now = new DateTime();
    return View::make('job.index', compact('jobs', 'now'));
  }
  /**
	 * Display a list of all jobs.
	 *
	 * @return Response
	 */
  public function listAll()
  {
    //
    $keyword = Input::get('keyword');
    $now = new DateTime();
    if (empty($keyword)) 
    {
      $jobs = Job::paginate(3);
      return View::make('job.listAll', array('jobs' => $jobs, 'keyword' => $keyword, 'now' => $now));
    }
    else
    {
      $jobs = Job::whereHas('user', function($q) use ($keyword)
                            {
                              return $q->whereRaw('industry like ?', array("%$keyword%"));
                            })       ->orWhereRaw('job_desc like ?', array("%$keyword%"))
        ->orWhereRaw('job_title like ?', array("%$keyword%"))
        ->orWhereRaw('location like ?', array("%$keyword%"))
        ->orWhereRaw('salary > ?', array($keyword))
        ->paginate(3);
      return View::make('job.listAll', array('jobs' => $jobs, 'keyword' => $keyword, 'now' => $now));
    }
  }

  /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  public function create()
  {
    //
    return View::make('job.create');
  }


  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function store()
  {
    //
    $input = Input::all();
    $job = new Job();
    $v = Validator::make($input, Job::$rules);
    if ($v->passes())
    {
      $job->job_title = $input['job_title'];
      $job->user_id = $input['user_id'];
      $job->job_desc = $input['job_desc'];
      $job->location = $input['location'];
      $job->salary = $input['salary'];
      $job->start_date = $input['start_date'];
      $job->final_date = $input['final_date'];
      $job->save();
      return Redirect::route('job.show', $job->id);
    }
    else
    {
      // Show validation errors
      if ($job)
      {
        return Redirect::route('job.create', array('job' => $job))->withErrors($v);
      }
      else
      {
        return Redirect::route('job.create');
      }
    }
  }

  /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function show($id)
  {
    //
    $jobs = Job::find($id);
    $now = new DateTime();
    $applications = $jobs->application;
    return View::make('job.show', compact('jobs', 'now', 'applications'));
  }


  /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function edit($id)
  {
    //
    $jobs = Job::find($id);
    $now = new Dateime();
    return View::make('job.edit', compact('jobs', 'now'));
  }


  /**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function update($id)
  {
    //
    $jobs = Job::find($id);
    $input = Input::all();
    $v = Validator::make($input, Job::$rules);
    if ($v->passes())
    {
      $jobs->job_title = $input['job_title'];
      $jobs->job_desc = $input['job_desc'];
      $jobs->location = $input['location'];
      $jobs->salary = $input['salary'];
      $jobs->save();
      return Redirect::route('job.show', $jobs->id);
    }
    else
    {
      // Show validation errors
      return Redirect::route('job.edit', $jobs->id)->withErrors($v);
    }
  }


  /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function destroy($id)
  {
    //
    $jobs = Job::find($id);
    $jobs->delete();
    return Redirect::route('job.index');
  }

}
