<?php

class UserController extends \BaseController {

  /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  public function index()
  {
    //
  }


  /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  public function create()
  {
    //
    $input = Input::all();
    $category = $input['category'];
    return View::make('user.create', compact('category'));
  }


  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function store()
  {
    //
    $input = Input::all();
    $category = $input['category'];
    $v = Validator::make($input, User::$create_rules);
    if ($v->passes())
    {
      $password = $input['password'];
      $encrypted = Hash::make($password);
      $user = new User;
      $user->username = $input['username'];
      $user->password = $encrypted;
      $user->name = $input['name'];
      $user->category = $input['category'];
      $user->email = $input['username'];
      $user->phone = $input['phone'];
      $user->industry = $input['industry'];
      $user->profilePic = $input['profilePic'];
      $user->description = $input['description'];
      $user->remember_token = "default"; 
      $user->save();

      return Redirect::route('user.login');
    }
    else
    {
      // Show validation errors
      return Redirect::route('user.create', compact('user', 'category'))->withErrors($v);
    }
  }
  /**
	 * login in function.
	 *
	 * @return Response
	 */
  public function login()
  {
    //
    $input = Input::all();
    $username = $input['username'];
    $password = $input['password'];
    $v = Validator::make($input, User::$login_rules);
    if (Auth::attempt(compact('username', 'password'))){
      return Redirect::to(URL::previous());
    }
    else
      return Redirect::route('job.index')->withErrors($v);

  }
  /**
	 * logout function.
	 *
	 * @return Response
	 */
  public function logout()
  {
    //
    Auth::logout();
    return Redirect::route('job.index');
  }



  /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function show($id)
  {
    //
    $user = User::find($id);
    $jobs = $user->jobs;
    $applications = $user->applications;
    return View::make('user.show', compact('user', 'applications', 'jobs'));

  }


  /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function edit($id)
  {
    //
    $user = User::find($id);
    return View::make('user.edit', compact('user'));
  }


  /**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function update($id)
  {
    //
    $user = User::find($id);
    $input = Input::all();
    $user->name = $input['name'];
    $user->description = $input['description'];
    $user->phone = $input['phone'];
    $user->profilePic = $input['profilePic'];
    $user->save();
    return Redirect::route('user.show', $user->id);
  }




  /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function destroy($id)
  {
    //
    $users = User::find($id);
    $users->delete();
    return Redirect::route('job.index');
  }


}
