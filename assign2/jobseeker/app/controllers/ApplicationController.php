<?php

class ApplicationController extends \BaseController {

  /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
  public function index()
  {
    //
  }


  /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  public function create()
  {
    //
    $input = Input::all();
    $id = $input['id'];
    $jobs = Job::find($id);
    $now = new DateTime();
    return View::make('application.create', compact('jobs', 'now'));

  }


  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function store()
  {
    //
    $input = Input::all();
    $application = new Application();
    $application->job_id = $input['job_id'];
    $application->user_id = $input['user_id'];
    $application->userName = $input['userName'];
    $application->employerName = $input['employerName'];
    $application->letter = $input['letter'];
    $application->apply_date = $input['apply_date'];
    $application->save();

    return Redirect::route('application.show',$application->id );

  }


  /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function show($id)
  {
    //
    $applications = Application::find($id);
    return View::make('application.show', compact('applications'));
  }


  /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function edit($id)
  {
    //
  }


  /**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function update($id)
  {
    //
  }


  /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function destroy($id)
  {
    //
  }


}
