<?php
/* 
 * Database access functions. 
 */
/* Show SQLite error. */
function show_error($e) {
  die("Error: " . $e->getMessage());
}

// Include your database access constants here
// require /* to be inserted /*;


function db_open()
{
  try {
    $db = new PDO('sqlite:db/jobdb.sqlite');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    die("Error: " . $e->getMessage());
  }
  return $db;
}

?>
