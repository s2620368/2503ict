<?php
/*
 * Functions for create, retieve, update and delete job listings.
 */

require "db_defs.php";
date_default_timezone_set('UTC');

/* FUNCTIONS FOR JOBS */

/* Gets job with the given id. */
function get_job($id) {
    try {
        $db = db_open();
        $sql = "select id, title, job_desc from jobs where id = :id";
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        
        $job = $statement->fetch();
        return $job;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* Gets a list of jobs */
function get_job_list($str) {
    try{
        $db = db_open();
      
        $sql = "select jobs.id, title from jobs, employer where ((industry like :str) or (job_desc like :str) or (location like :str) or (salary > :sal))
                     and jobs.employer_id = employer.id";
        
        $statement = $db->prepare($sql);
        $statement->bindValue(':str', "%$str%");
        $statement->bindValue(':sal',$str);
        $statement->execute();
        $jobs = $statement->fetchAll();
        return $jobs;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
/* Gets detail of a particular job */
function get_job_detail($id) {
    try {
        $db = db_open();
        $sql = "select jobs.id, employer_id, title, job_desc, location, salary, empl_name from jobs, employer where jobs.id = :id and jobs.employer_id = employer.id";
      // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $job = $statement->fetch();
        
        return $job;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}

/* FUNCTIONS FOR EMPLOYERS */
/* Gets list of Employers. */
function get_employer_list($str) {
    try{
        $db = db_open();
        $sql = "select id, empl_name from employer ";
        if ($str) {
            $sql .= "where empl_name like :str ";
        }
        $sql .=  "order by id";
        // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        if ($str){
            $statement->bindValue(':str', "%$str%");
        }
        $statement->execute();
    
      $employers = $statement->fetchAll();
        return $employers;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
/* Gets Employer detail with the given id. */
function get_employer_detail($id) {
    try {
        $db = db_open();
        $sql = "select id, empl_name, empl_desc, industry from employer where id = :id";
      // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $employers = $statement->fetch();
        
        return $employers;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
/* Gets the employer Job List */
function get_empl_job_list($id) {
    try{
        $db = db_open();
        $sql = "select jobs.id, title from jobs, employer where jobs.employer_id = employer.id and jobs.employer_id = :id ";
        $statement = $db->prepare($sql);
          if ($id){
            $statement->bindValue(':id', $id);
          }
        $statement->execute();
        $jobs = $statement->fetchAll();
        return $jobs;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
/* Adds a new job from form data and returns its id. */
function add_job($title, $employer_id, $job_desc, $location, $salary) {
    try {
        $db = db_open();
        $sql = "INSERT INTO jobs(employer_id, title, job_desc, location, salary)
	VALUES (:employer_id, :title, :job_desc, :location, :salary)";
        $statement = $db->prepare($sql);
        $statement->bindValue(':title', $title);
        $statement->bindValue(':employer_id', $employer_id);
        $statement->bindValue(':job_desc', $job_desc);
        $statement->bindValue(':location', $location);
        $statement->bindValue(':salary', $salary);
        $statement->execute();
        $id = $db->lastInsertId();
    } catch(PDOException $e) {
        die("Error: " . $e->getMessage());
    }
    return $id;
}


/*Gets employer id to redirect to employer job list page after deleting a job NOT WORKING*/
function get_id($job_id){
try {
        $db = db_open();
        $sql = "select id, employer_id from jobs where id = :id";
      // print "$sql<br>\n";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $job_id);
        $statement->execute();
        $id = $statement->fetch();
        return $id;

    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
  
}

/* Updates a job with the given id using the given title. job desc. */
function update_job($id,$title,$salary,$location,$job_desc) {
  try {
    $db = db_open();
    $sql = "update jobs set title = :title, salary = :salary, location = :location, job_desc = :job_desc where id = :id";
    $statement = $db->prepare($sql);
    $statement->bindValue(':title', $title);
    $statement->bindValue(':salary', $salary);
    $statement->bindValue(':location', $location);
    $statement->bindValue(':job_desc', $job_desc);
    $statement->bindValue(':id', $id);
    $statement->execute();
    
  }catch (PDOException $e) {
        die("Error: " . $e->getMessage());
  }
}

/* Deletes the item with the given id. */
function delete_item($id) {
  try{
    $db = db_open();
    $sql = "delete from jobs where id = :id";
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    
  }catch (PDOException $e) {
        die("Error: " . $e->getMessage());
  }
}

