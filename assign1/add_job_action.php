<?php
/*
 * Adds new item from form data.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";



# Get form data
$employer_id = $_POST['id'];
$title = $_POST['title'];
$job_desc= $_POST['job_desc'];
$location= $_POST['location'];
$salary= $_POST['salary'];

# Check data is valid
# Check data is valid
if (empty($title)) {
    $error = "Title must not be empty.";
    header("Location: add_job.php?id=$employer_id&error=$error");
    exit;
}

if (empty($salary)) {
    $error = "Salary must not be empty.";
    header("Location: add_job.php?id=$employer_id&error=$error");
    exit;
}
if (empty($location)) {
    $error = "Location must not be empty.";
    header("Location: add_job.php?id=$employer_id&error=$error");
    exit;
}
if (empty($job_desc)) {
    $error = "Job Description must not be empty.";
    header("Location: add_job.php?id=$employer_id&error=$error");
    exit;
}

# add new item with form data
$id = add_job($title, $employer_id, $job_desc, $location, $salary );
if ($id) {
header("Location: employer_job_detail.php?id=$id"); 
exit;
}

?>
