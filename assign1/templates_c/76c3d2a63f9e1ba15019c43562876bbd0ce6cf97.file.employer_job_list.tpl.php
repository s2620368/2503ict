<?php /* Smarty version Smarty-3.1.16, created on 2014-04-14 06:24:36
         compiled from "./templates/employer_job_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1979942827534b7f24dd0177-55215145%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76c3d2a63f9e1ba15019c43562876bbd0ce6cf97' => 
    array (
      0 => './templates/employer_job_list.tpl',
      1 => 1397454953,
      2 => 'file',
    ),
    '841e5708e91187e043b81d7a1aa47a16f057dcc3' => 
    array (
      0 => './templates/layout_empl.tpl',
      1 => 1397294532,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1979942827534b7f24dd0177-55215145',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534b7f24e39b61_96874227',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534b7f24e39b61_96874227')) {function content_534b7f24e39b61_96874227($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Seek</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Job Seek</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="employers.php">List all Employers</a>
            <a class="list-group-item" href="add_job.php">Advertise a Job</a>
          </div>
        </div>
        
  <div class="col-sm-9">
    <h1 id="heading"> Employer Detail </h1>

<h1><?php echo $_smarty_tpl->tpl_vars['employer']->value['empl_name'];?>
</h1>
    
<p>
<?php echo $_smarty_tpl->tpl_vars['employer']->value['empl_desc'];?>


<p>
<p>
<?php echo $_smarty_tpl->tpl_vars['employer']->value['industry'];?>


<p>
<?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['jobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
    <li><a href="employer_job_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['job']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
<?php } ?>

  </div>
    



      </div>
    </div>
  
</body>
</html><?php }} ?>
