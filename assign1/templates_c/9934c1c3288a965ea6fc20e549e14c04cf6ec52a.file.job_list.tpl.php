<?php /* Smarty version Smarty-3.1.16, created on 2014-05-25 10:41:38
         compiled from "./templates/job_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9599633225348ff1e31f605-83668879%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9934c1c3288a965ea6fc20e549e14c04cf6ec52a' => 
    array (
      0 => './templates/job_list.tpl',
      1 => 1398342160,
      2 => 'file',
    ),
    '6a7af4126964f0058b10d90b95735a781583c43d' => 
    array (
      0 => './templates/layout_users.tpl',
      1 => 1400024046,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9599633225348ff1e31f605-83668879',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5348ff1e3c8f31_52876417',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5348ff1e3c8f31_52876417')) {function content_5348ff1e3c8f31_52876417($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oz Vacancies</title>

  
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="styles/style.css">
  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Oz Vacancies</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="job_list.php">List all Jobs</a>
            
          </div>
        </div>
        
  <div class="col-sm-9">
    <?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
      <h1 id="heading">Jobs found for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h1>
      <?php } else { ?>
      <h1 id="heading">Job Vacancies</h1>
    <?php }?>
    <br>
    <?php if ($_smarty_tpl->tpl_vars['jobs']->value) {?>
      <ol>
        <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['jobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
          <li ><a class="job-list" href="job_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['job']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
        <?php } ?>
      </ol>
    <?php } else { ?>
      <p>No items found.</p>
    <?php }?>
    <br>
    <br>
    <p>Search for industry, location, minimum salary or job key word: </p>
    <form method="get" action="job_list.php">Search: <input type="text" name="query"> <input type="submit" value="Go">
    </form>
     
  </div>

        <br>
      <br>
      <br>
      <br>    
      <footer>
        Angela King s2620368<br>
        2503ICT Web Programming Assignment 1
      </footer> 
        
      </div>
    </div>
  
</body>
</html><?php }} ?>
