<?php /* Smarty version Smarty-3.1.16, created on 2014-04-24 13:29:19
         compiled from "./templates/job_detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21456996375349022a8c0ec0-25390285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64656cf620c6432cf848bbce3c44998042e5d598' => 
    array (
      0 => './templates/job_detail.tpl',
      1 => 1398344251,
      2 => 'file',
    ),
    '6a7af4126964f0058b10d90b95735a781583c43d' => 
    array (
      0 => './templates/layout_users.tpl',
      1 => 1398335530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21456996375349022a8c0ec0-25390285',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5349022a9248f8_70568027',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5349022a9248f8_70568027')) {function content_5349022a9248f8_70568027($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oz Vacancies</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Oz Vacancies</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="job_list.php">List all Jobs</a>
            
          </div>
        </div>
        
  <div class="col-sm-9">
    <h1 id="heading"> Job Detail </h1>
      <DL>
        <DT>Job Title<DD class="job-detail"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
<p>
        <DT>Employer Name<DD class="job-detail"><?php echo $_smarty_tpl->tpl_vars['job']->value['empl_name'];?>
<p>
        <DT>Job Description<DD class="job-detail"><?php echo $_smarty_tpl->tpl_vars['job']->value['job_desc'];?>
<p>
        <DT>Job Location<DD class="job-detail"><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
<p>
        <DT>Salary<DD class="job-detail"><?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
<p>
      </DL>  
  </div>

        <br>
      <br>
      <br>
      <br>    
      <footer>
        Angela King s2620368<br>
        2503ICT Web Programming Assignment 1
      </footer> 
        
      </div>
    </div>
  
</body>
</html><?php }} ?>
