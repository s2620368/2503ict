<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 23:18:22
         compiled from "./templates/add_job.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97239705553462634d47543-60587986%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cfb4e49f4f0ae6adbf94b4b0e30221d4d80ad85b' => 
    array (
      0 => './templates/add_job.tpl',
      1 => 1398347574,
      2 => 'file',
    ),
    'b46d1259264d0803c44c60b005127b2fd3fdface' => 
    array (
      0 => './templates/employer_detail.tpl',
      1 => 1398348615,
      2 => 'file',
    ),
    '841e5708e91187e043b81d7a1aa47a16f057dcc3' => 
    array (
      0 => './templates/layout_empl.tpl',
      1 => 1398812710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97239705553462634d47543-60587986',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53462634db7813_71141805',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53462634db7813_71141805')) {function content_53462634db7813_71141805($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Oz Vacancies</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Oz Vacancies</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="employers.php">List all Employers</a>
          </div>
        </div>
        
<div class="col-sm-9">

          <h2 id="heading">Add a new Job</h2><br>
          <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p>
          <?php }?>
          <form class="form-horizontal" role="form" method="post" action="add_job_action.php">
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['employer']->value['id'];?>
">
            <div class="form-group">
              <label class="col-sm-2"> Title </label>
              <div class="col-sm-10"> <input type="text" name="title"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Salary </label>
              <div class="col-sm-10"> <input type="text" name="salary"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Location </label>
              <div class="col-sm-10"> <input type="text" name="location"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Job Description </label>
              <div class="col-sm-10"> <textarea name="job_desc"></textarea> </div>
            </div>
            <button type="submit" class="btn btn-success"> Sumbit Job </button>
          </form>
  
        </div>



        <br>
      <br>
      <br>
      <br>    
      <footer>
        Angela King s2620368<br>
        2503ICT Web Programming Assignment 1
      </footer> 
      </div>
    </div>
  
</body>
</html><?php }} ?>
