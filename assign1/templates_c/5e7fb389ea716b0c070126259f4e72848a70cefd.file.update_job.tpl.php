<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 23:19:06
         compiled from "./templates/update_job.tpl" */ ?>
<?php /*%%SmartyHeaderCode:375659445534e63f296d7a2-64282278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e7fb389ea716b0c070126259f4e72848a70cefd' => 
    array (
      0 => './templates/update_job.tpl',
      1 => 1398347273,
      2 => 'file',
    ),
    'b46d1259264d0803c44c60b005127b2fd3fdface' => 
    array (
      0 => './templates/employer_detail.tpl',
      1 => 1398348615,
      2 => 'file',
    ),
    '841e5708e91187e043b81d7a1aa47a16f057dcc3' => 
    array (
      0 => './templates/layout_empl.tpl',
      1 => 1398812710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '375659445534e63f296d7a2-64282278',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534e63f29ea572_54073649',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534e63f29ea572_54073649')) {function content_534e63f29ea572_54073649($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Oz Vacancies</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Oz Vacancies</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="employers.php">List all Employers</a>
          </div>
        </div>
        
  <div class="col-sm-9">
    <h1 id="heading"> Update Job </h1><p><br>
    <?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
    <p><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p>
<?php }?>
      <form method="post" action="update_job_action.php">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
">
          <table>
            <tr><td>Title:</td> <td><input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
"><p></td></tr>
            <tr><td>Salary:</td> <td><input type="text" name="salary" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
"><p></td></tr>
            <tr><td>Location:</td> <td><input type="text" name="location" value="<?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
"><p></td></tr>
            <tr><td>Job Description:</td> <td><textarea name="job_desc">"<?php echo $_smarty_tpl->tpl_vars['job']->value['job_desc'];?>
"</textarea><p></td></tr>
            <tr></tr>
            <tr><td><input type="submit" value="Update Job" class="update-btn"></td></tr>
          </table>
      </form>
    
  </div>

        <br>
      <br>
      <br>
      <br>    
      <footer>
        Angela King s2620368<br>
        2503ICT Web Programming Assignment 1
      </footer> 
      </div>
    </div>
  
</body>
</html><?php }} ?>
