<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 23:10:02
         compiled from "./templates/employers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8140462885346397126b313-84010819%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f631e88d6c9bde0a3685c5cc9927ef539917f8fc' => 
    array (
      0 => './templates/employers.tpl',
      1 => 1398341849,
      2 => 'file',
    ),
    '841e5708e91187e043b81d7a1aa47a16f057dcc3' => 
    array (
      0 => './templates/layout_empl.tpl',
      1 => 1398812710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8140462885346397126b313-84010819',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534639712de525_73701217',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534639712de525_73701217')) {function content_534639712de525_73701217($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Oz Vacancies</title>

  <link rel="stylesheet" href="styles/style.css">
    <!-- Bootstrap -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
<body>

  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Oz Vacancies</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="employers.php">Employers</a></li>
            <li><a href="docs/doc.html">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="employers.php">List all Employers</a>
          </div>
        </div>
        
  <div class="col-sm-9">
    
    <h1 id="heading"> Employers List </h1>
    <br>
    <?php  $_smarty_tpl->tpl_vars['employer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['employer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['employer']->key => $_smarty_tpl->tpl_vars['employer']->value) {
$_smarty_tpl->tpl_vars['employer']->_loop = true;
?>
      <ul id="employer-list">
        <li><a href="employer_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['employer']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['employer']->value['empl_name'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
      </ul>
    <?php } ?>
  
  </div>

        <br>
      <br>
      <br>
      <br>    
      <footer>
        Angela King s2620368<br>
        2503ICT Web Programming Assignment 1
      </footer> 
      </div>
    </div>
  
</body>
</html><?php }} ?>
