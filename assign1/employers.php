<?php
/* 
 * Home page of PM database search example, using Smarty templates. 
 * BAD STYLE: Does not sanitise user input.
 */
date_default_timezone_set('UTC');
include '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

if (isset($_GET['query'])) {
    $query = $_GET['query'];
} else {
    $query = "";
}

$employers = get_employer_list($query);

$smarty = new Smarty;
$smarty->assign("query",$query);
$smarty->assign("employers",$employers);
$smarty->display("employers.tpl");
?>