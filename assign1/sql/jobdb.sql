drop table if exists empl_list;
drop table if exists job_list;


CREATE TABLE IF NOT EXISTS employer (
    id integer not null primary key autoincrement,
    empl_name varchar(80) not null,
    industry varchar(80) not null,
    empl_desc text default ''
);

CREATE TABLE IF NOT EXISTS jobs (
    id integer not null primary key autoincrement,
    employer_id varchar(80) not null,
    title varchar(80) not null,
    job_desc text default '',
    location varchar(80) not null,
    salary integer
);

INSERT INTO employer(empl_name, industry, empl_desc)
	VALUES ("The Bacon Factory", "Food Production" , "All your poultry, meat, smallgoods in one place");

INSERT INTO employer(empl_name, industry, empl_desc)
	VALUES ("Sell Your House Real Estate", "Real Estate", "Professional Realters to buy, sell or rent your home");

INSERT INTO jobs(employer_id, title, job_desc, location, salary) 
  VALUES ("2", "Realtor", "Postion available for a full time Agent", "Nerang", 40000);
  
INSERT INTO jobs(employer_id, title, job_desc, location, salary) 
  VALUES ("2", "Receptionist", "Postion available for a full time Receptionist", "Nerang", 25000);

INSERT INTO jobs(employer_id, title, job_desc, location, salary)
	VALUES ("1", "Butcher", "Postion available for a qualified Butcher ", "Broadbeach", 50000);

INSERT INTO jobs(employer_id, title, job_desc, location, salary)
	VALUES ("1", "Butcher Apprentice", "Postion available for an Apprentice Butcher ", "Broadbeach", 35000);
