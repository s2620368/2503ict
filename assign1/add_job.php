<?php
/*
 * Displays form to add new item.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

date_default_timezone_set('UTC');

if (isset($_GET['error'])) {
    $error = $_GET['error']; # for error reporting
} else {
    $error = "";
}

$id = $_GET['id'];
$employer = get_employer_detail($id);

$smarty = new Smarty;
$smarty->assign("employer",$employer);
$smarty->assign('error',$error);
$smarty->display('add_job.tpl');
?>
