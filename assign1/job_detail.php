<?php
/*
 * Displays details of item with given id.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

date_default_timezone_set('UTC');

$id = $_GET['id'];
$job = get_job_detail($id);

$smarty = new Smarty;
$smarty->assign("job",$job);
$smarty->display("job_detail.tpl");
?>
