{extends file='employer_detail.tpl'}
{block name=title}Job Search – Add Job{/block}

{block name=body1}
<div class="col-sm-9">

          <h2 id="heading">Add a new Job</h2><br>
          {if $error}
            <p>{$error}</p>
          {/if}
          <form class="form-horizontal" role="form" method="post" action="add_job_action.php">
            <input type="hidden" name="id" value="{$employer.id}">
            <div class="form-group">
              <label class="col-sm-2"> Title </label>
              <div class="col-sm-10"> <input type="text" name="title"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Salary </label>
              <div class="col-sm-10"> <input type="text" name="salary"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Location </label>
              <div class="col-sm-10"> <input type="text" name="location"> </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2"> Job Description </label>
              <div class="col-sm-10"> <textarea name="job_desc"></textarea> </div>
            </div>
            <button type="submit" class="btn btn-success"> Sumbit Job </button>
          </form>
  
        </div>


{/block}