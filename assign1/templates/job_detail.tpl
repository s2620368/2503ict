{extends file='layout_users.tpl'}
{block name=title}Job Seek - Home{/block}

{block name=body1}
  <div class="col-sm-9">
    <h1 id="heading"> Job Detail </h1>
      <DL>
        <DT>Job Title<DD class="job-detail">{$job.title}<p>
        <DT>Employer Name<DD class="job-detail">{$job.empl_name}<p>
        <DT>Job Description<DD class="job-detail">{$job.job_desc}<p>
        <DT>Job Location<DD class="job-detail">{$job.location}<p>
        <DT>Salary<DD class="job-detail">{$job.salary}<p>
      </DL>  
  </div>
{/block}
