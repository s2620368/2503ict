{extends file='layout_empl.tpl'}
{block name=title}Job Seek - Employers Home{/block}

{block name=body1}
  <div class="col-sm-9">
    
    <h1 id="heading"> Employers List </h1>
    <br>
    {foreach $employers as $employer}
      <ul id="employer-list">
        <li><a href="employer_detail.php?id={$employer.id}">{$employer.empl_name|escape}</a></li>
      </ul>
    {/foreach}
  
  </div>
{/block}

