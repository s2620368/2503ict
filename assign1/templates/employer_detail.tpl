{extends file='layout_empl.tpl'}
{block name=title}Job Seek - Employer Detail{/block}

{block name=body1}
  <div class="col-sm-9">
    <h1 id="heading"> Employer Detail </h1>
      <dl>
        <dt><h2>{$employer.empl_name}</h2><p>
        <dt>Description<dd>{$employer.empl_desc}<p>
        <dt>Employers Industry<dd>{$employer.industry}<p>
      </dl>
          <ol>
            {foreach $empl_jobs as $empl_job}
              <li><a href="employer_job_detail.php?id={$empl_job.id}">{$empl_job.title|escape}</a></li>
            {/foreach}
          </ol>
    <p>
    <p><a href="add_job.php?id={$employer.id}">Add a new Job</a></p>
    <br>
      
  </div>
    


{/block}

