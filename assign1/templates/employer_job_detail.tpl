{extends file='employer_detail.tpl'}
{block name=title}Job Seek - Employer Detail{/block}

{block name=body1}
  <div class="col-sm-9">
      <h1 id="heading"> Employer Job Detail </h1><br>
        <DL>
          <DT>Job Title<DD class="job-detail">{$empl_jobs.title}<p>
          <DT>Job Description<DD class="job-detail">{$empl_jobs.job_desc}<p>
          <DT>Job Location<DD class="job-detail">{$empl_jobs.location}<p>
          <DT>Salary<DD class="job-detail">{$empl_jobs.salary}<p>
        </DL> 
      <a href="delete_job_action.php?id={$empl_jobs.id}" class="btn btn-warning btn-xs active" role="button">Delete this Job</a><br>
      <p><br>
      <a href="update_job.php?id={$empl_jobs.id}" class="btn btn-warning btn-xs active" role="button">Update this Job</a>
          
      <br><p><br>
      <a id="employer-detail" href="employer_detail.php?id={$empl_jobs.employer_id}">back to job list</a>
      
          
  </div>         
{/block}