{extends file='employer_detail.tpl'}


{block name=body1}
  <div class="col-sm-9">
    <h1 id="heading"> Update Job </h1><p><br>
    {if $error}
    <p>{$error}</p>
{/if}
      <form method="post" action="update_job_action.php">
        <input type="hidden" name="id" value="{$job.id}">
          <table>
            <tr><td>Title:</td> <td><input type="text" name="title" value="{$job.title}"><p></td></tr>
            <tr><td>Salary:</td> <td><input type="text" name="salary" value="{$job.salary}"><p></td></tr>
            <tr><td>Location:</td> <td><input type="text" name="location" value="{$job.location}"><p></td></tr>
            <tr><td>Job Description:</td> <td><textarea name="job_desc">"{$job.job_desc}"</textarea><p></td></tr>
            <tr></tr>
            <tr><td><input type="submit" value="Update Job" class="update-btn"></td></tr>
          </table>
      </form>
    
  </div>
{/block}
