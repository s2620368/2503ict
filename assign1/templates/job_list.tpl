{extends file='layout_users.tpl'}
{block name=title}Job Seek - Home{/block}

{block name=body1}
  <div class="col-sm-9">
    {if $query}
      <h1 id="heading">Jobs found for '{$query}'</h1>
      {else}
      <h1 id="heading">Job Vacancies</h1>
    {/if}
    <br>
    {if $jobs}
      <ol>
        {foreach $jobs as $job}
          <li ><a class="job-list" href="job_detail.php?id={$job.id}">{$job.title|escape}</a></li>
        {/foreach}
      </ol>
    {else}
      <p>No items found.</p>
    {/if}
    <br>
    <br>
    <p>Search for industry, location, minimum salary or job key word: </p>
    <form method="get" action="job_list.php">Search: <input type="text" name="query"> <input type="submit" value="Go">
    </form>
     
  </div>
{/block}