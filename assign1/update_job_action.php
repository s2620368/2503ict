<?php
/*
 * Updates item from form data.
 */

require "includes/defs.php";

# Get form data
$id = $_POST['id'];
$title = $_POST['title'];
$salary = $_POST['salary'];
$location = $_POST['location'];
$job_desc = $_POST['job_desc'];

$employer = get_employer_detail($id);

# Check data is valid
if (empty($title)) {
    $error = "Title must not be empty.";
    header("Location: update_job.php?id=$id&error=$error");
    exit;
}

if (empty($salary)) {
    $error = "Salary must not be empty.";
    header("Location: update_job.php?id=$id&error=$error");
    exit;
}
if (empty($location)) {
    $error = "Location must not be empty.";
    header("Location: update_job.php?id=$id&error=$error");
    exit;
}
if (empty($job_desc)) {
    $error = "Job Description must not be empty.";
    header("Location: update_job.php?id=$id&error=$error");
    exit;
}

# Perform update with data
update_job($id,$title,$salary,$location,$job_desc);

header("Location: employer_job_detail.php?id=$id"); 
exit;
?>
