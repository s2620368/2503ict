<?php
/*
 * Displays details of item with given id.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

date_default_timezone_set('UTC');

$id = $_GET['id'];

$employer = get_employer_detail($id);
$empl_jobs = get_empl_job_list($id);

$smarty = new Smarty;
$smarty->assign("employer",$employer);
$smarty->assign("empl_jobs",$empl_jobs);
$smarty->display("employer_detail.tpl");
?>