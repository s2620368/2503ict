<?php
/*
 * Deletes item with given id.
 */
require "includes/defs.php";

$job_id = $_GET['id'];

delete_item($job_id);
$id = get_id($job_id);

header("Location: employers.php");
exit;
?>
