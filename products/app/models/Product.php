<?php

class Product extends Eloquent {
  
  public static $rules = array(
    'name' => 'required',
    'price' => array('required', 'min:4')
  );
  
}