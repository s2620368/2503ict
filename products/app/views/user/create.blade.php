@extends('product.layout')

@section('content')

<h1>User page</h1>

      {{ Form::open(array('action' => 'UserController@store')); }}
      {{ Form::text('username', null, array('class' => 'input-small', 'placeholder' => 'Email')); }}
      {{ Form::password('password', array('class' => 'input-small', 'placeholder' => 'Password')); }}
      {{ Form::submit('Create Account'); }}
      {{ Form::close(); }}
      
{{ link_to_route('home', 'Home') }}

@stop