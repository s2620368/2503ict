@extends('product.layout')

@section('content')

<h1>Product</h1>

<p>Name: {{{$product->name}}}</p>
<p>Price: {{{$product->price}}}</p>
<p>{{link_to_route('product.edit', 'Edit', array($product->id)) }} </p>
<p>{{link_to_route('product.index', 'Home')}}
  
<p>
  {{ Form::open(array('method' => 'DELETE', 'route' =>array('product.destroy', $product->id))) }}
  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
  {{ Form::close() }}

@stop