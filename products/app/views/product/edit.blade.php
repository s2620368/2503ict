@extends('product.layout')

@section('content')

<h1>Edit Product</h1>

{{ Form::model($product, array('method' => 'PUT', 'route' =>array('product.update', $product->id))) }}

{{ Form::label('name', 'Product Name:') }}
{{ Form::text('name') }}
{{ $errors->first('name') }}
<p>
{{ Form::label('price', 'Price:') }}
{{ Form::text('price') }}
{{ $errors->first('price') }}
<p>
{{ Form::submit('Update') }}

{{ Form::close() }}




@stop