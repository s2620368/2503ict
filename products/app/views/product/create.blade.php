@extends('product.layout')

@section('content')

<h1>Create Product</h1>

{{ Form::open(array('action' => 'ProductController@store')) }}

{{ Form::label('name', 'Product Name:') }}
{{ Form::text('name') }}
<p>
{{ Form::label('price', 'Price:') }}
{{ Form::text('price') }}
<p>
{{ Form::submit('Create') }}

{{ Form::close() }}
  
<p>{{ link_to_route('home', 'Home') }}

@stop