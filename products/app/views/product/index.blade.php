@extends('product.layout')

@section('title')
Products
@stop

@section('content')
<h1>Products</h1><p>

  <ul>
    @foreach($products as $product)
      <li> {{ link_to_route('product.show',$product->name, array($product->id)) }}
    @endforeach
  </ul>
<p>{{ link_to_route('product.create','Add a new Product', '',array('class' => 'btn btn-default') ) }}
  

  
@stop