<html>
  <head>
     <title>@yield('title')</title>
  </head>
  <body>
    @if(Auth::check())
      <p style="font-weight:bold">Hi {{ Auth::user()->username }}!<p>
      <p style="font-weight:bold">{{ link_to_route('user.logout', 'Logout (' . Auth::user()->username . ')') }}
    @else 
      {{ Form::open(array('action' => 'UserController@login')) }}
      {{ Form::text('username', null, array('class' => 'input-small', 'placeholder' => 'Email')); }}
      {{ Form::password('password', array('class' => 'input-small', 'placeholder' => 'Password')); }}
      {{ Form::submit('Sign in'); }}
      {{ Form::close(); }}
      <p>{{$errors->first('username')}}
      <p>{{$errors->first('password')}}
    @endif
    <p>{{ link_to_route('user.create', 'Create an Account') }}
    @yield('content')
  </body>
</html>