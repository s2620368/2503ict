<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
    return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
    $input = Input::all();
    $v = Validator::make($input, User::$create_rules);
    if ($v->passes())
    {
      $password = $input['password'];
      $encrypted = Hash::make($password);
      $user = new User;
      $user->username = $input['username'];
      $user->password = $encrypted;
      $user->remember_token = "default";
      $user->save();
      return Redirect::route('product.index');
    }
    else
    {
      // Show validation errors
      return Redirect::route('user.create')->withErrors($v);
    }
  }
  /**
	 * login in function.
	 *
	 * @return Response
	 */
	public function login()
	{
		//
     $input = Input::all();
     $username = $input['username'];
     $password = $input['password'];
     $v = Validator::make($input, User::$login_rules);
     if (Auth::attempt(compact('username', 'password'))){
           return Redirect::to(URL::previous());
           }
           else
           return Redirect::route('product.index')->withErrors($v);
       
    
	}
  /**
	 * logout function.
	 *
	 * @return Response
	 */
	public function logout()
	{
		//
    Auth::logout();
    return Redirect::route('product.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
