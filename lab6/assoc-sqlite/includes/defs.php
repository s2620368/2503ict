<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";
require "db_defs.php";

/* Search sample data for form data $query in name, year or state. */
function search($query) {
   try {
     $db = db_open();
     $sql = "select * from pms where (name like :query) or (state like :query) or (start like :query) or (finish like :query) ";
     $statement = $db->prepare($sql);
     $statement->bindValue(':query', "%$query%");
     $statement->execute();
     $pms = $statement->fetchAll();
     
    
  } catch(PDOException $e) {
    die("Error: ".$e->getMessage());
  }
  return $pms;
  /*


   global $pms; 
  
  $query = trim($query);
  
  // Filter $pms by $query
  if (!empty($query)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['name'], $query) !== FALSE ||
          strpos($pm['from'], $query) !== FALSE || 
          strpos($pm['to'], $query) !== FALSE ||
          stripos($pm['state'], $query) !== FALSE) {
        $results[] = $pm;
      }
    }
    return $results;
  } else {
    return $pms;
  }*/
}

?>
