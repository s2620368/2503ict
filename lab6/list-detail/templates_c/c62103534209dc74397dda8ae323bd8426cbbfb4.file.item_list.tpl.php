<?php /* Smarty version Smarty-3.1.16, created on 2014-04-09 02:54:49
         compiled from "./templates/item_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3011469475344b679b61887-36879639%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c62103534209dc74397dda8ae323bd8426cbbfb4' => 
    array (
      0 => './templates/item_list.tpl',
      1 => 1396997695,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3011469475344b679b61887-36879639',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'query' => 0,
    'items' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5344b679c5cd55_72909593',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5344b679c5cd55_72909593')) {function content_5344b679c5cd55_72909593($_smarty_tpl) {?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<title>Show item list</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/wp.css">
</head>

<body>
<?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
<h1>Items for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h1>
<?php } else { ?>
<h1>Items</h1>
<?php }?>
    
<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
<ul>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
    <li><a href="item_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['summary'], ENT_QUOTES, 'UTF-8', true);?>
</a></li>
<?php } ?>
</ul>
<?php } else { ?>
<p>No items found.</p>
<?php }?>

<form method="get" action="item_list.php">
Search: <input type="text" name="query"> <input type="submit" value="Go">
</form>

<p><a href="add_item.php">Add a new item</a></p>

<?php if ($_smarty_tpl->tpl_vars['query']->value) {?>
<p><a href="item_list.php">Home</a></p>
<?php }?>
</body>
</html>
<?php }} ?>
